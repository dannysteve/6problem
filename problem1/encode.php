<?php
	
	$testMessage = strtolower($_POST['message']);

	$huruf_hidup = ['a', 'i', 'u', 'e', 'o'];

	$split_message = str_split($testMessage);

	$message_baru = "";
	$cek_hidup = false;
	$temporary_huruf_mati = [];


	foreach($split_message as $key => $message) {
		foreach($huruf_hidup as $hidup) {
			if($hidup == $message) {
				$cek_hidup = true;
				break;
			} else {
				$cek_hidup = false;
			}
		}

		if ($cek_hidup) {
			$message_baru .= strtoupper($message);
		} else {
			if (empty($temporary_huruf_mati)) {
				array_push($temporary_huruf_mati, $message);
				
			} else {
				if (!in_array($message, $temporary_huruf_mati)) {
					array_push($temporary_huruf_mati, $message);
				}
			}
			$message_baru .= $key . '_';
		}
	}

	echo $message_baru;
?>