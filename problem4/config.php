<?php
/*
 * Basic Site Settings and API Configuration
 */

// Database configuration
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'loginphp');
define('DB_USER_TBL', 'users');

// Google API configuration
define('GOOGLE_CLIENT_ID', '68257333790-s7ijdrjm38uhj2h1h93np045pj7kguh1.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET', 'CGjMzqTeYEm6UMUUrwbJe-yE');
define('GOOGLE_REDIRECT_URL', 'http://localhost/6problem/problem4/main.php');

// Start session
if(!session_id()){
    session_start();
}

// Include Google API client library
require_once __DIR__ . '/google-api-php-client/vendor/autoload.php';
// require_once __DIR__ . '/google-api-php-client/contrib/Google_Oauth2Service.php';

// Call Google API
$gClient = new Google_Client();
$gClient->setApplicationName('Login localhost');
$gClient->setClientId(GOOGLE_CLIENT_ID);
$gClient->setClientSecret(GOOGLE_CLIENT_SECRET);
$gClient->setRedirectUri(GOOGLE_REDIRECT_URL);
$gClient->addScope('https://mail.google.com/');

$google_oauthV2 = new Google_Service_Plus($gClient);
