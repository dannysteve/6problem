<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<style type="text/css">
	</style>
</head>
<body>

	<div class="container h-100 d-flex justify-content-center">
		<div id="" style="">
			<form class="" action="" method="" style="">
				<div class="form-group">
					<label for="">Please input text below</label>
					<input type="email" class="form-control message" id="">
				</div>
				<div class="form-group">
					<input type="button" name="encode" class="encode_button" value="ENCODE">
					<input type="button" name="decode" class="decode_button" value="DECODE">
					<span class="result">HASILNYA AKAN MUNCUL DISINI</span>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		var message = '';
		$('.encode_button').on('click', function() {
			message = $('.message').val();

			$.ajax({
		        url: "/6problem/problem1/encode.php",
		        type: "POST",
		        data: {
		        	message
		        },
		        success: function (response) {
		           // you will get response from your php page (what you echo or print)                 
		           $('.result').text(response);
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
			})
		})

		$('.decode_button').on('click', function() {
			$.ajax({
		        url: "/6problem/problem2/decode.php",
		        type: "POST",
		        data: {
		        	message
		        },
		        success: function (response) {
		           // you will get response from your php page (what you echo or print)                 
		           $('.result').text(response);
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
			})

		})
	</script>
</body>
</html>